FROM amd64/node:22.14.0@sha256:32ad68e28d3eb06c388fc9d87f2143c2df157d4e99ade9024189513238c0b838
ENV AUDIENCE="https://shiny.unbound.se"
ENV ORIGIN_HOST="auth0mock"
ENV ORIGIN="https://auth0mock:3333"
EXPOSE 3333
WORKDIR /app
ADD package.json yarn.lock /app/
RUN yarn install --frozen-lockfile
ADD *.js /app/
ADD public /app/public
RUN mkdir -p /root/.config
ENTRYPOINT ["yarn", "start"]
