const fs = require('fs')

const setup = (usersFile) => {
  let users = {}
  if (fs.existsSync(usersFile)) {
    console.log(`initial users file "${usersFile}" exists, reading`)
    const read = fs.readFileSync(usersFile, { encoding: 'utf8', flag: 'r' })
    users = JSON.parse(read)
    for (let key of Object.keys(users)) {
      users[key] = { ...users[key], email: key }
    }
    console.log('users:', users)
  } else {
    console.log(`initial users file "${usersFile}" missing`)
  }
  return users
}
module.exports = setup
